package br.com.rhfa.avenuechallenge

import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseView
import br.com.rhfa.avenuechallenge.mvp.presenter.MainPresenter
import br.com.rhfa.avenuechallenge.mvp.view.MainView
import br.com.rhfa.avenuechallenge.views.activities.AbstractBottomNavigationBarActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AbstractBottomNavigationBarActivity<MainPresenter>(), MainView {

    override fun newPresenter(): MainPresenter {
        return MainPresenter()
    }

    override fun myOnCreate() {
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getBaseView(): BaseView {
        return this
    }

    override fun showProgressBar(visibility: Int) {

    }
}
