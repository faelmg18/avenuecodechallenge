package br.com.rhfa.avenuechallenge.network

import br.com.rhfa.avenuechallenge.communication.BaseAPIClient
import br.com.rhfa.avenuechallenge.communication.rest.APIClientResponseListener
import br.com.rhfa.avenuechallenge.communication.rest.ApiClientException
import br.com.rhfa.avenuechallenge.communication.sevices.ShowService
import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.mvp.presenter.ShowsPresenter

import retrofit2.Call

class ShowServiceImpl(private val showsPresenter: ShowsPresenter) : BaseAPIClient<ShowService>(showsPresenter) {

    fun doGetListShow(page: Int) {
        val call = `interface`.doGetListShows(page)
        execute(call, object : APIClientResponseListener<List<Show>> {

            override fun onError(obj: Call<List<Show>>, t: ApiClientException) {
                if (t.code == 504) {
                    showsPresenter.onTimeOut(t.message!!)
                    return
                }
                showsPresenter.onError(t.message!!)
            }

            override fun onSuccess(obj: List<Show>) {
                showsPresenter.updateListRecycler(obj)
            }
        })
    }

    companion object {

        @Volatile
        private var instance: ShowServiceImpl? = null

        fun newInstance(showsPresenter: ShowsPresenter): ShowServiceImpl? {
            instance = ShowServiceImpl(showsPresenter)
            return instance
        }
    }
}