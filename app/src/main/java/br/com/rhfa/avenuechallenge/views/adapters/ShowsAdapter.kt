package br.com.rhfa.avenuechallenge.views.adapters

import android.view.View

import br.com.rhfa.avenuechallenge.R
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter
import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.persistence.FavoritesRepository
import br.com.rhfa.avenuechallenge.persistence.RepositoryFactory
import br.com.rhfa.avenuechallenge.views.adapters.viewholders.ShowsViewHolder

class ShowsAdapter(dataList: MutableList<Show>?, basePresenterImpl: BasePresenter) :
        BaseRecyclerViewAdapter<Show, ShowsViewHolder>(dataList, basePresenterImpl) {

    override fun layoutId(): Int {
        return R.layout.shows_items
    }

    override fun myOnCreateViewHolder(parent: View, viewType: Int, mBasePresenterImpl: BasePresenter): ShowsViewHolder {
        return ShowsViewHolder(parent, mBasePresenterImpl)
    }

    override fun myOnBindViewHolder(holder: ShowsViewHolder, position: Int, item: Show) {
        holder.bind(item, basePresenterImpl.getActivity()!!)
        holder?.itemView.setOnClickListener {
            onItemClick?.invoke(item)
        }
    }
}
