package br.com.rhfa.avenuechallenge.views.activities

import android.view.View
import br.com.rhfa.avenuechallenge.R
import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseView
import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.mvp.presenter.MainPresenter
import br.com.rhfa.avenuechallenge.mvp.view.MainView
import br.com.rhfa.avenuechallenge.util.Utilities
import com.nostra13.universalimageloader.core.ImageLoader
import kotlinx.android.synthetic.main.detail_activity.*

class DetailsActivity : BaseActivity<MainPresenter>(), MainView {

    val imageLoader = ImageLoader.getInstance()

    override fun getLayoutId(): Int {
        return R.layout.detail_activity
    }

    override fun myOnCreate() {

        addBackButton()

        val show = intent?.extras?.getSerializable("value") as Show

        title = show.name

        summary.text = Utilities.fromHtml(show.summary)
        imdb.text = show.externalInfo.imdb

        if (!show.isFavorite) {
            favorite.visibility = View.GONE
        }

        imageLoader.displayImage(show.image.original, show_image)

    }

    override fun getBaseView(): BaseView {
        return this
    }

    override fun newPresenter(): MainPresenter {
        return MainPresenter()
    }
}