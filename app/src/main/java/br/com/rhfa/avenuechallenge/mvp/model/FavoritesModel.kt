package br.com.rhfa.avenuechallenge.mvp.model

import br.com.rhfa.avenuechallenge.domain.Show

interface FavoritesModel {
    fun saveOrRemoveFavorites(item: Show): Boolean
    fun isFavorites(item: Show): Boolean
    fun getAll()
}