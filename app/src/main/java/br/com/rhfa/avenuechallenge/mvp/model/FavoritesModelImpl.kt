package br.com.rhfa.avenuechallenge.mvp.model

import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.mvp.presenter.FavoritesPresenterImpl
import br.com.rhfa.avenuechallenge.persistence.RepositoryFactory

class FavoritesModelImpl(var favoritesPresenterImpl: FavoritesPresenterImpl?) : FavoritesModel {

    var favoritesRepository = RepositoryFactory.getInstance().createFavoritesRepository()

    override fun saveOrRemoveFavorites(item: Show): Boolean {

        var success = true
        var result: Long
        if (item.isFavorite) {
            result = favoritesRepository.save(item)
            if (result < 0) {
                success = false
            }
        } else {
            result = favoritesRepository.delete(item)
            if (result < 0) {
                success = false
            }
        }
        return success
    }

    override fun isFavorites(item: Show): Boolean {
        var itemRetrieved = favoritesRepository.retrieveFavoritesById(item.id)
        var isFavorite = false
        if (itemRetrieved != null) {
            isFavorite = true
        }
        return isFavorite
    }

    override fun getAll() {
        favoritesPresenterImpl?.showProgressBar(true)
        var list = favoritesRepository.retrieveAllFavorites() as List<Show>
        favoritesPresenterImpl?.showProgressBar(false)
        favoritesPresenterImpl?.updateListRecycler(list)
    }

}