package br.com.rhfa.avenuechallenge.domain;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

import br.com.rhfa.avenuechallenge.database.EntitiePersistable;
import br.com.rhfa.avenuechallenge.database.annotation.PrimaryKey;
import br.com.rhfa.avenuechallenge.database.annotation.SaveAsBytes;

public class Show implements EntitiePersistable {

    @PrimaryKey
    private long id;
    private String url;
    private String name;
    private String type;
    private String language;
    @SaveAsBytes
    private List<String> genres;
    private String status;
    private int runtime;
    private String premiered;
    public String officialSite;
    @SerializedName("network")
    @SaveAsBytes
    private Channel airChannel;
    @SerializedName("webChannel")
    @SaveAsBytes
    private Channel webChannel;
    @SaveAsBytes
    private Image image;
    @SerializedName("externals")
    @SaveAsBytes
    private ExternalInfo externalInfo;
    private String summary;
    @SaveAsBytes
    private Map<String, String> rating;
    private boolean isFavorite;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public String getPremiered() {
        return premiered;
    }

    public void setPremiered(String premiered) {
        this.premiered = premiered;
    }

    public String getOfficialSite() {
        return officialSite;
    }

    public void setOfficialSite(String officialSite) {
        this.officialSite = officialSite;
    }

    public Channel getAirChannel() {
        return airChannel;
    }

    public void setAirChannel(Channel airChannel) {
        this.airChannel = airChannel;
    }

    public Channel getWebChannel() {
        return webChannel;
    }

    public void setWebChannel(Channel webChannel) {
        this.webChannel = webChannel;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public ExternalInfo getExternalInfo() {
        return externalInfo;
    }

    public void setExternalInfo(ExternalInfo externalInfo) {
        this.externalInfo = externalInfo;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Map<String, String> getRating() {
        return rating;
    }

    public void setRating(Map<String, String> rating) {
        this.rating = rating;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean favorite) {
        isFavorite = favorite;
    }
}
