package br.com.rhfa.avenuechallenge.communication.rest

import android.app.Activity
import br.com.rhfa.avenuechallenge.views.activities.BaseActivity
import java.io.Serializable

interface RHFViewInterface : Serializable {
    fun runOnUiThread(action: Runnable)
    fun getActivity(): Activity?
}
