package br.com.rhfa.avenuechallenge.views.adapters.viewholders

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter

abstract class AbstractViewHolder<T>(itemView: View, val presenter: BasePresenter) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(item: T, context: Context)

    fun getViewRoot(): View {
        return itemView
    }
}
