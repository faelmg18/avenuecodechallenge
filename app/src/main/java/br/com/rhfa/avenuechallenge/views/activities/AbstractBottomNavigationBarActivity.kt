package br.com.rhfa.avenuechallenge.views.activities

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import br.com.rhfa.avenuechallenge.R
import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseViewFragment
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter
import br.com.rhfa.avenuechallenge.views.fragments.FavoritesFragment
import br.com.rhfa.avenuechallenge.views.fragments.ShowsFragment
import kotlinx.android.synthetic.main.activity_main.*


abstract class AbstractBottomNavigationBarActivity<T : BasePresenter> : BaseActivity<T>() {

    private var currentFragment: BaseViewFragment? = null
    var lastItemIdMenuSelected: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigation.selectedItemId = R.id.navigation_shows
    }

    protected val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_shows -> {
                title = getString(R.string.tv_show_title)
                gotoFragment(ShowsFragment::class.java, null, item.itemId)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorites -> {
                title = getString(R.string.favorites_title)
                gotoFragment(FavoritesFragment::class.java, null, item.itemId)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    protected fun gotoFragment(toFragment: Class<out Fragment>, bundle: Bundle?, itemId: Int) {

        if (lastItemIdMenuSelected == itemId) {
            return
        }

        var fragment: Fragment? = null
        try {
            fragment = toFragment.newInstance()

            var mBundle: Bundle? = null

            if (bundle == null) {
                mBundle = Bundle()
            }
            fragment!!.arguments = mBundle

        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }

        if (fragment == null)
            return

        currentFragment = fragment as BaseViewFragment?


        lastItemIdMenuSelected = itemId
        supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit()
    }

    fun getCurrentFragment(): BaseViewFragment? {
        return currentFragment
    }

    override fun getFragment(): BaseViewFragment? {
        return currentFragment
    }
}