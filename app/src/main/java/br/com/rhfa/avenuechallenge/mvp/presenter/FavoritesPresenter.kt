package br.com.rhfa.avenuechallenge.mvp.presenter

import android.os.Bundle
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter
import br.com.rhfa.avenuechallenge.domain.Show


interface FavoritesPresenter : BasePresenter {
    fun getFavoritesShows(savedInstanceState: Bundle?)
    fun updateListRecycler(items: List<Show>)
    fun updateItemRecycler(item: Show)
    fun getFavoritesList(): MutableList<Show>
}