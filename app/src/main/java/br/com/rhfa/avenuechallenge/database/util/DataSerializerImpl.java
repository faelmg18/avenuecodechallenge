package br.com.rhfa.avenuechallenge.database.util;

import android.telecom.Conference;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rafael.alves on 11/06/2018.
 */

public class DataSerializerImpl implements DataSerializer {

    public ObjectMapper objectMapper = null;
    private static DataSerializerImpl instance = null;
    public static final String DEFAULT_FORMAT_DATE = "yyyy-MM-dd HH:mm a z";

    private DataSerializerImpl(DataSerializerMapperConfiguration dataSerializerMapperConfiguration) {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        DateFormat df = new SimpleDateFormat(dataSerializerMapperConfiguration.getFormatDate());
        objectMapper.setDateFormat(df);
    }

    public void setDateFormat(DateFormat df) {
        objectMapper.setDateFormat(df);
    }

    private DataSerializerImpl() {
        this(new DataSerializerMapperConfiguration(DEFAULT_FORMAT_DATE));
    }

    public static DataSerializerImpl getInstance(DataSerializerMapperConfiguration dataSerializerMapperConfiguration) {
        if (instance == null) {
            instance = new DataSerializerImpl(dataSerializerMapperConfiguration);
        }

        return instance;
    }

    public static DataSerializerImpl getInstance() {
        if (instance == null) {
            instance = new DataSerializerImpl();
        }

        return instance;
    }

    @Override
    public String toJson(Object content) {

        try {
            return objectMapper.writeValueAsString(content);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T toObject(String json, Class<T> targetClass) {
        try {
            return (T) objectMapper.readValue(json, targetClass);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T toObject(String json, final TypeReference<T> type) {
        try {
            return (T) objectMapper.readValue(json, type);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public <T> List<T> toObject(String jsonAnswer, CollectionType collectionType) {

        try {
            return (List<T>) objectMapper.readValue(jsonAnswer, collectionType);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public <T> List<T> toObjectList(String jsonAnswer, Class<T> targetClass) {
        try {
            return (List<T>) objectMapper.readValue(jsonAnswer, TypeFactory.defaultInstance().constructCollectionType(List.class,
                    targetClass));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public <T> ArrayList<T> toObjectArrayList(String jsonAnswer, Class<T> targetClass) {
        try {
            return (ArrayList<T>) objectMapper.readValue(jsonAnswer, TypeFactory.defaultInstance().constructCollectionType(List.class,
                    targetClass));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String shouldSerializeAHashMapObjectIntoAJson(Object value) {
        try {
            ObjectWriter prettyPrinter = new ObjectMapper().writerWithDefaultPrettyPrinter();
            String prettyJson = prettyPrinter.writeValueAsString(value);

            System.out.println(prettyJson);

            return prettyJson;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }
}
