package br.com.rhfa.avenuechallenge.views.adapters

import android.view.View
import br.com.rhfa.avenuechallenge.R
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter
import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.views.adapters.viewholders.FavoritesShowsViewHolder

class FavoriteShowsAdapter(dataList: MutableList<Show>, basePresenterImpl: BasePresenter) :
        BaseRecyclerViewAdapter<Show, FavoritesShowsViewHolder>(dataList, basePresenterImpl) {
    override fun layoutId(): Int {
        return R.layout.shows_items
    }

    override fun myOnCreateViewHolder(parent: View, viewType: Int, mBasePresenterImpl: BasePresenter): FavoritesShowsViewHolder {
        return FavoritesShowsViewHolder(parent, mBasePresenterImpl)
    }

    override fun myOnBindViewHolder(holder: FavoritesShowsViewHolder, position: Int, item: Show) {
        holder.bind(item, basePresenterImpl.getActivity()!!)
        holder?.itemView.setOnClickListener {
            onItemClick?.invoke(item)
        }
    }
}