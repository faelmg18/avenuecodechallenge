package br.com.rhfa.avenuechallenge.views.adapters.viewholders

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import br.com.rhfa.avenuechallenge.R
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter
import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.mvp.presenter.FavoritesPresenterImpl
import br.com.rhfa.avenuechallenge.views.components.DialogBuilder
import com.nostra13.universalimageloader.core.ImageLoader

class FavoritesShowsViewHolder(itemView: View, presenter: BasePresenter) : AbstractViewHolder<Show>(itemView, presenter) {

    private var imageLoader = ImageLoader.getInstance()

    override fun bind(item: Show, context: Context) {
        val textViewTitle = itemView.findViewById<TextView>(R.id.textViewTitle)
        val imageViewPoster = itemView.findViewById<TextView>(R.id.imageViewPlaceHolder) as ImageView
        val imageViewDelete = itemView.findViewById<TextView>(R.id.imageViewDelete) as ImageView
        val imageViewSave = itemView.findViewById<TextView>(R.id.imageViewSave) as ImageView
        textViewTitle.text = item.name


        imageViewDelete.visibility = View.VISIBLE
        imageViewSave.visibility = View.GONE

        imageViewDelete.setOnClickListener {
            DialogBuilder.createDialogYesOrNoDecision(context, context.getString(R.string.remove_favorite), object : DialogBuilder.ButtonCallback() {
                override fun onPositive(builder: AlertDialog.Builder, dialogInterface: DialogInterface) {
                    item.setIsFavorite(false)
                    getShowPresenter()?.updateItemRecycler(item)
                }
            })
        }
        imageViewSave.setOnClickListener({
            item.setIsFavorite(true)
            getShowPresenter()?.updateItemRecycler(item)
            getShowPresenter()?.showToast(context.getString(R.string.successfully_saved_to_favorites))
        })

        imageLoader.displayImage(item.image?.medium, imageViewPoster)
    }

    fun getShowPresenter(): FavoritesPresenterImpl? {
        if (presenter is FavoritesPresenterImpl) {
            return (presenter as FavoritesPresenterImpl)
        }
        return null
    }
}
