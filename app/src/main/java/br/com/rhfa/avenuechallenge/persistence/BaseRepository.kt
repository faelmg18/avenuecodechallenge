package br.com.rhfa.avenuechallenge.persistence

interface BaseRepository<T> {
    fun deleteAll()

    fun count(): Int

}
