package br.com.rhfa.avenuechallenge.mvp.presenter

import android.app.Activity
import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseView
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter
import br.com.rhfa.avenuechallenge.mvp.view.MainView
import br.com.rhfa.avenuechallenge.views.activities.BaseActivity

class MainPresenter : BasePresenter {

    private var view: MainView? = null

    override fun setView(view: BaseView) {
        this.view = view as MainView
    }

    override fun runOnUiThread(action: Runnable) {
        view!!.runOnUiThread(action)
    }

    override fun getActivity(): BaseActivity<*>? {
        return view as BaseActivity<*>?
    }

    override fun showToast(message: String) {
        view!!.showToast(message)
    }

    override fun showProgressBar(visible: Boolean) {

    }
}
