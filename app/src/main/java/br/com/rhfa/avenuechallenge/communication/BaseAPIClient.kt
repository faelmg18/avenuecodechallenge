package br.com.rhfa.avenuechallenge.communication

import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter
import br.com.rhfa.avenuechallenge.communication.rest.AbstractAPIClient
import br.com.rhfa.avenuechallenge.communication.rest.RHFViewInterface

open class BaseAPIClient<T>(rhfViewInterface: RHFViewInterface) : AbstractAPIClient<T>(rhfViewInterface) {

    val base: BasePresenter = rhfViewInterface as BasePresenter

    override fun getBaseUrl(): String {
        return "https://api.tvmaze.com/"
    }

    override fun onEnd() {
        base.showProgressBar(false)
    }

    override fun onStart() {
        base.showProgressBar(true)
    }
}
