package br.com.rhfa.avenuechallenge.views.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import br.com.rhfa.avenuechallenge.MainActivity
import br.com.rhfa.avenuechallenge.R
import br.com.rhfa.avenuechallenge.mvp.presenter.FavoritesPresenter
import br.com.rhfa.avenuechallenge.mvp.presenter.FavoritesPresenterImpl
import br.com.rhfa.avenuechallenge.mvp.view.FavoriteView
import br.com.rhfa.avenuechallenge.views.activities.DetailsActivity
import br.com.rhfa.avenuechallenge.views.adapters.FavoriteShowsAdapter
import br.com.rhfa.avenuechallenge.views.components.DialogBuilder
import kotlinx.android.synthetic.main.favorite_shows_fragment.*

class FavoritesFragment : BaseFragments<FavoritesPresenter, MainActivity>(), FavoriteView {

    var adapter: FavoriteShowsAdapter? = null

    override fun layoutId(): Int {
        return R.layout.favorite_shows_fragment
    }

    override fun newPresenter(): FavoritesPresenter {
        return FavoritesPresenterImpl()
    }

    override fun myOnCreate(view: View, savedInstanceState: Bundle?) {

    }

    override fun myOnViewCreate(view: View, savedInstanceState: Bundle?) {
        showsFavorites.layoutManager = LinearLayoutManager(activity)
        adapter = FavoriteShowsAdapter(presenter?.getFavoritesList()!!, presenter!!)

        adapter?.onItemClick = { item ->
            item.setIsFavorite(true)
            gotoNextScreen(DetailsActivity::class.java, item)
        }

        showsFavorites.adapter = adapter
        presenter?.getFavoritesShows(savedInstanceState)
    }

    override fun updateListRecycler() {
        adapter?.notifyDataSetChanged()
    }

    override fun updateItemRecycler(posicao: Int) {
        adapter?.removeItem(posicao)
        adapter?.notifyItemChanged(posicao)
    }

    override fun showProgressBar(visibility: Int) {
        progressBar.visibility = visibility
    }

    override fun showErrorSaveOrDeleteTvOnFavorite() {
        DialogBuilder.createDialogYesOrNoDecision(activity,
                getString(R.string.something_went_wrong),
                getString(R.string.on_error_save_or_delete_tv_show_favorite),
                R.string.retry,
                R.string.cancel,
                object : DialogBuilder.ButtonCallback() {
                    override fun onPositive(builder: AlertDialog.Builder?, dialogInterface: DialogInterface?) {
                        presenter?.getFavoritesShows(null)
                    }
                })
    }
}