package br.com.rhfa.avenuechallenge.mvp.model

interface ShowsModel {
    fun getShows(page: Int)
}