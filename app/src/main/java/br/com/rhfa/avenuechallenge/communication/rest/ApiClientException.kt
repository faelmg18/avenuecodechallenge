package br.com.rhfa.avenuechallenge.communication.rest

class ApiClientException : Exception {

    var code: Int = 0
        private set

    internal constructor(message: String, code: Int) : super(message) {
        this.code = code
    }

    internal constructor(expection: Throwable, code: Int) : super(expection) {
        this.code = code
    }

    internal constructor(message: String) : super(message) {}

    internal constructor(expection: Throwable) : super(expection) {}
}
