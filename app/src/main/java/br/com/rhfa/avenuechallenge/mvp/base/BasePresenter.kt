package br.com.rhfa.avenuechallenge.communication.mvp.basee

import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseView
import br.com.rhfa.avenuechallenge.communication.rest.RHFViewInterface


interface BasePresenter : RHFViewInterface {
    fun setView(view: BaseView)

    fun showToast(message: String)

    fun showProgressBar(visible: Boolean)
}