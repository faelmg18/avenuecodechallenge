package br.com.rhfa.avenuechallenge.mvp.presenter

import android.app.Activity
import android.os.Bundle
import android.view.View
import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseView
import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.mvp.model.FavoritesModel
import br.com.rhfa.avenuechallenge.mvp.model.FavoritesModelImpl
import br.com.rhfa.avenuechallenge.mvp.model.ShowsModel
import br.com.rhfa.avenuechallenge.mvp.model.ShowsModelImpl
import br.com.rhfa.avenuechallenge.mvp.view.ShowView


class ShowPresenterImpl : ShowsPresenter {

    var model: ShowsModel = ShowsModelImpl(this)
    var favoriteModel: FavoritesModel = FavoritesModelImpl(null)

    var view: ShowView? = null
    var items: ArrayList<Show> = ArrayList()
    var currentPage: Int = 0

    override fun isFavorite(item: Show): Boolean {
        return favoriteModel.isFavorites(item)
    }

    override fun getShows(savedInstanceState: Bundle?) {
        getShows()
    }

    override fun getShowsList(): List<Show> {
        return items
    }

    private fun getShows() {
        model.getShows(currentPage)
    }

    override fun updatePage() {
        currentPage++
        getShows()
    }

    override fun updateListRecycler(items: List<Show>) {
        if (currentPage == 0) {
            this.items.clear()
        }

        this.items.addAll(items)
        view?.updateListRecycler()
    }

    override fun updateItemRecycler(item: Show) {
        for (i in 0 until this.items.size) {
            if (this.items[i].id === item.id) {
                item.setIsFavorite(item.isFavorite)
                if (!favoriteModel.saveOrRemoveFavorites(item)) {
                    view?.showErrorSaveOrDeleteTvOnFavorite()
                    break
                }
                view?.updateItemRecycler(i)
                break
            }
        }
    }

    override fun onError(message: String) {
        view?.showDialogOnErrorServer()
    }

    override fun setView(view: BaseView) {
        this.view = view as ShowView
    }

    override fun showToast(message: String) {
        view?.showToast(message)
    }

    override fun showProgressBar(visible: Boolean) {
        val visibility = if (visible) View.VISIBLE else View.GONE
        view!!.showProgressBar(visibility)
    }

    override fun runOnUiThread(action: Runnable) {
        view?.runOnUiThread(action)
    }

    override fun getActivity(): Activity? {
        return view?.getActivity()
    }

    override fun onTimeOut(message: String) {
        view?.showDialogOnTimeOut()
    }
}