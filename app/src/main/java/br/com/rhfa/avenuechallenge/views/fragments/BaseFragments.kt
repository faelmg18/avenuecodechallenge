package br.com.rhfa.avenuechallenge.views.fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseView
import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseViewFragment
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter
import br.com.rhfa.avenuechallenge.views.activities.BaseActivity
import java.io.Serializable

abstract class BaseFragments<T : BasePresenter, AC : BaseActivity<*>> : Fragment(), BaseViewFragment {

    abstract fun layoutId(): Int

    var presenter: T? = null
        private set

    var ac: AC? = null

    lateinit var mView: View

    protected val activityBase: BaseView?
        get() = activity as BaseView?

    protected abstract fun newPresenter(): T

    internal abstract fun myOnCreate(view: View, savedInstanceState: Bundle?)

    internal abstract fun myOnViewCreate(view: View, savedInstanceState: Bundle?)

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mView = inflater.inflate(layoutId(),
                container, false)

        presenter = newPresenter()

        presenter!!.setView(this)
        myOnCreate(mView, savedInstanceState)

        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myOnViewCreate(view, savedInstanceState)

    }

    fun findViewById(resourceId: Int): View {
        return mView.findViewById(resourceId)
    }

    override fun showProgressBar(visibility: Int) {

    }

    override fun runOnUiThread(action: Runnable) {
        if (activityBase != null) {
            activityBase!!.runOnUiThread(action)
        }
    }

    override fun gotoNextScreen(cls: Class<*>) {
        val intent = Intent(activity, cls)
        startActivity(intent)
    }

    override fun gotoNextScreen(cls: Class<*>, requestCode: Int) {
        val intent = Intent(activity, cls)
        startActivityForResult(intent, requestCode)
    }

    override fun <T : Serializable> gotoNextScreen(aClass: Class<*>, obj: T) {
        val bundle = Bundle()
        bundle.putSerializable("value", obj)
        gotoNextScreen(aClass, bundle)
    }

    override fun gotoNextScreen(cls: Class<*>, dataBundle: String, keyBundle: String) {
        val intent = Intent(activity, cls)
        val bundle = Bundle()
        bundle.putString(keyBundle, dataBundle)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun gotoNextScreen(cls: Class<*>, bundle: Bundle) {
        val intent = Intent(activity, cls)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun gotoNextScreen(cls: Class<*>, bundle: Bundle, requestCode: Int) {
        val intent = Intent(activity, cls)
        intent.putExtras(bundle)
        activity!!.startActivityForResult(intent, requestCode)
    }

    fun getMActivity(): AC? {
        return activity as AC
    }

    override fun getFragment(): BaseViewFragment? {
        return getMActivity()?.getFragment()
    }

    override fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }
}