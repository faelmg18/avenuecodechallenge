package br.com.rhfa.avenuechallenge.communication.sevices

import br.com.rhfa.avenuechallenge.domain.Show
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ShowService {

    @GET("shows")
    fun doGetListShows(
            @Query("page") page: Int): Call<List<Show>>
}
