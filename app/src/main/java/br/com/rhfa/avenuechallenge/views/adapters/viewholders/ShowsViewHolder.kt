package br.com.rhfa.avenuechallenge.views.adapters.viewholders

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import br.com.rhfa.avenuechallenge.R
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter
import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.mvp.presenter.ShowsPresenter
import br.com.rhfa.avenuechallenge.views.components.DialogBuilder
import com.nostra13.universalimageloader.core.ImageLoader

class ShowsViewHolder(itemView: View, presenter: BasePresenter) : AbstractViewHolder<Show>(itemView, presenter) {

    private val imageLoader = ImageLoader.getInstance()

    override fun bind(item: Show, context: Context) {
        val textViewTitle = itemView.findViewById<TextView>(R.id.textViewTitle)
        val imageViewPoster = itemView.findViewById<TextView>(R.id.imageViewPlaceHolder) as ImageView
        val imageViewDelete = itemView.findViewById<TextView>(R.id.imageViewDelete) as ImageView
        val imageViewSave = itemView.findViewById<TextView>(R.id.imageViewSave) as ImageView
        textViewTitle.text = item.name
        item.setIsFavorite(item.isFavorite || getShowPresenter()?.isFavorite(item)!!)

        if (item.isFavorite ) {
            imageViewDelete.visibility = View.VISIBLE
            imageViewSave.visibility = View.GONE
        } else {
            imageViewDelete.visibility = View.GONE
            imageViewSave.visibility = View.VISIBLE
        }

        imageViewDelete.setOnClickListener {
            DialogBuilder.createDialogYesOrNoDecision(context, context.getString(R.string.remove_favorite),
                    object : DialogBuilder.ButtonCallback() {
                        override fun onPositive(builder: AlertDialog.Builder, dialogInterface: DialogInterface) {
                            item.setIsFavorite(false)
                            getShowPresenter()?.updateItemRecycler(item)
                        }
                    })
        }
        imageViewSave.setOnClickListener({
            item.setIsFavorite(true)
            getShowPresenter()?.updateItemRecycler(item)
            getShowPresenter()?.showToast(context.getString(R.string.successfully_saved_to_favorites))
        })

        imageLoader.displayImage(item.image?.medium, imageViewPoster)
    }

    fun getShowPresenter(): ShowsPresenter? {
        if (presenter is ShowsPresenter) {
            return (presenter as ShowsPresenter)
        }
        return null
    }
}
