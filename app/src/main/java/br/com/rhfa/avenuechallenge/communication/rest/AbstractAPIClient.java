package br.com.rhfa.avenuechallenge.communication.rest;


import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import br.com.rhfa.avenuechallenge.communication.dataserializer.DataSerializer;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class AbstractAPIClient<I> {

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static volatile Retrofit retrofit = null;

    protected abstract void onStart() throws Exception;

    protected abstract void onEnd() throws Exception;

    protected abstract String getBaseUrl();

    private I mInterface;

    private WeakReference<RHFViewInterface> viewInterfaceWeakReference;
    private Gson gson;

    public AbstractAPIClient(RHFViewInterface activity) {
        this.viewInterfaceWeakReference = new WeakReference<>(activity);
        initClient();
    }

    private void initClient() {

        HttpLoggingInterceptor interceptor = getHttpLoggingInterceptor();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(interceptor);
        setTimeout(builder);
        builder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request = getHeaders(original);
                return chain.proceed(request);
            }
        });

        OkHttpClient client = builder.build();
        gson = getGson();
        retrofit = builder(client, getConvertFactory());

        Type type = getClass().getGenericSuperclass();
        Type t = ((ParameterizedType) type).getActualTypeArguments()[0];
        createInterface((Class<I>) t);
    }

    private Retrofit builder(OkHttpClient client, Converter.Factory factory) {
        return new Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addConverterFactory(factory)
                .client(client)
                .build();
    }

    protected Converter.Factory getConvertFactory() {
        return getGsonConverterFactory(gson);
    }

    private Converter.Factory getGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    private Gson getGson() {
        return new GsonBuilder().setDateFormat(getPattern()).create();
    }

    private String getPattern() {
        return DEFAULT_DATE_FORMAT;
    }

    private void createInterface(Class<I> tClass) {
        mInterface = retrofit.create(tClass);
    }

    public I getInterface() {
        return mInterface;
    }

    protected void setTimeout(OkHttpClient.Builder builder) {
        builder.connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);
    }

    protected HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        interceptor.setLevel
                (HttpLoggingInterceptor.Level.BODY);
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        return interceptor;
    }

    protected Request getHeaders(Request original) {
        return original.newBuilder()
                .header("Content-type", "application/json")
                .method(original.method(), original.body())
                .build();
    }

    protected <T> void execute(final Call<T> call, final APIClientResponseListener listener) {
        startProcess();
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(final Call<T> call, final Response<T> response) {
                Log.d("AbstractAPIClient", "AbstractAPIClient Success => code = " + response.code() + "");
                if (viewInterfaceWeakReference != null && viewInterfaceWeakReference.get() != null) {
                    viewInterfaceWeakReference.get().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (response.code() != 200) {
                                listener.onError(call, new ApiClientException("Internal server error "
                                        + response.message(), response.code()));
                                return;
                            }

                            listener.onSuccess(response.body());
                        }
                    });
                    finishProcess();
                }
            }

            @Override
            public void onFailure(final Call<T> call, final Throwable t) {
                call.cancel();
                Log.d("AbstractAPIClient", "AbstractAPIClient Failure, motive is = " + t.getMessage());
                if (viewInterfaceWeakReference != null) {
                    viewInterfaceWeakReference.get().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listener.onError(call, new ApiClientException(t));
                        }
                    });
                }
                finishProcess();
            }
        });
    }

    private void startProcess() {
        try {
            onStart();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void finishProcess() {
        try {
            onEnd();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected RequestBody createBody(String json, MediaType mediaType) {
        RequestBody body = RequestBody.create(mediaType, json);
        return body;
    }

    protected <T> RequestBody createBody(T obj) {
        String json = DataSerializer.getInstance().toJson(obj);
        return createBody(json, MediaType.parse("application/json; charset=utf-8"));
    }

    protected <T> RequestBody createBody(T obj, MediaType mediaType) {
        String json = DataSerializer.getInstance().toJson(obj);
        return createBody(json, mediaType);
    }

    protected RequestBody createBody(HashMap<String, String> map, MediaType type) {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(type);

        for (String key : map.keySet()) {
            builder.addFormDataPart(key, map.get(key));
        }

        RequestBody requestBody = builder.build();
        return requestBody;
    }

    protected RequestBody createBody(HashMap<String, String> map) {
        return createBody(map, MultipartBody.FORM);
    }

    protected Activity getActivity() {
        return (viewInterfaceWeakReference != null && viewInterfaceWeakReference.get() != null) ?
                viewInterfaceWeakReference.get().getActivity() : null;
    }

    public WeakReference<RHFViewInterface> getViewInterfaceWeakReference() {
        return viewInterfaceWeakReference;
    }

    protected static Retrofit getRetrofit() {
        return retrofit;
    }

    public abstract class BodyCreatedListener {
        protected void onBodyCreated(RequestBody body) {
        }
    }
}
