package br.com.rhfa.avenuechallenge.mvp.view

import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseView

interface ShowView : BaseView {
    fun updateListRecycler()
    fun updateItemRecycler(posicao: Int)
    fun showErrorSaveOrDeleteTvOnFavorite()
    fun showDialogOnErrorServer()
    fun showDialogOnTimeOut()
}