package br.com.rhfa.avenuechallenge.mvp.presenter

import android.os.Bundle
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter
import br.com.rhfa.avenuechallenge.domain.Show


interface ShowsPresenter : BasePresenter {
    fun getShows(savedInstanceState: Bundle?)
    fun updatePage()
    fun updateListRecycler(items: List<Show>)
    fun updateItemRecycler(item: Show)
    fun isFavorite(item: Show): Boolean
    fun getShowsList(): List<Show>
    fun onError(message: String)
    fun onTimeOut(message: String)
}