package br.com.rhfa.avenuechallenge.domain;

import br.com.rhfa.avenuechallenge.database.EntitiePersistable;

public class Image implements EntitiePersistable {

    private String medium;

    private String original;

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
}
