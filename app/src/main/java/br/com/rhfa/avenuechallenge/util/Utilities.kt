package br.com.rhfa.avenuechallenge.util

import android.app.Activity
import android.content.Context
import android.os.Build
import android.support.v4.content.ContextCompat
import android.text.Html
import android.text.Spanned
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager

import br.com.rhfa.avenuechallenge.ApplicationContext


object Utilities {

    fun fromHtml(html: String): Spanned {
        val result: Spanned
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            result = Html.fromHtml(html)
        }
        return result
    }


    fun isNullOrEmpty(value: String?): Boolean {
        return if (value == null || value == "null" || value.isEmpty() || value == "" || value.trim { it <= ' ' }.isEmpty()) {
            true
        } else false

    }

    fun getColor(context: Context, color: Int): Int {

        try {
            return ContextCompat.getColor(context, color)

        } catch (e: Exception) {
            return color
        }

    }

    fun closeKeyboard(activity: Activity) {
        val view = activity.currentFocus
        val imm = ApplicationContext.appContext!!
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (view != null && view.windowToken != null) {
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun forceCloseKeyboard(activity: Activity) {
        activity.window.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )
    }
}


