package br.com.rhfa.avenuechallenge.domain;

import br.com.rhfa.avenuechallenge.database.EntitiePersistable;

public class ExternalInfo implements EntitiePersistable {

    private String tvrage;
    private long thetvdb;
    private String imdb;

    public String getTvrage() {
        return tvrage;
    }

    public void setTvrage(String tvrage) {
        this.tvrage = tvrage;
    }

    public long getThetvdb() {
        return thetvdb;
    }

    public void setThetvdb(long thetvdb) {
        this.thetvdb = thetvdb;
    }

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }
}
