package br.com.rhfa.avenuechallenge.views.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import br.com.rhfa.avenuechallenge.MainActivity
import br.com.rhfa.avenuechallenge.R
import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.mvp.presenter.ShowPresenterImpl
import br.com.rhfa.avenuechallenge.mvp.presenter.ShowsPresenter
import br.com.rhfa.avenuechallenge.mvp.view.ShowView
import br.com.rhfa.avenuechallenge.views.activities.DetailsActivity
import br.com.rhfa.avenuechallenge.views.adapters.ShowsAdapter
import br.com.rhfa.avenuechallenge.views.components.DialogBuilder
import br.com.rhfa.avenuechallenge.views.components.OnLoadMoreListener
import kotlinx.android.synthetic.main.shows_fragment.*

class ShowsFragment : BaseFragments<ShowsPresenter, MainActivity>(), ShowView {

    private var adapter: ShowsAdapter? = null

    override fun layoutId(): Int {
        return R.layout.shows_fragment
    }

    override fun newPresenter(): ShowsPresenter {
        return ShowPresenterImpl()
    }

    override fun showProgressBar(visibility: Int) {
        progressBar.visibility = visibility
    }

    override fun myOnCreate(view: View, savedInstanceState: Bundle?) {
    }

    override fun myOnViewCreate(view: View, savedInstanceState: Bundle?) {
        presenter?.getShows(savedInstanceState)

        adapter = ShowsAdapter(presenter?.getShowsList() as MutableList<Show>?, presenter!!)
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)

        adapter?.onItemClick = { item ->
            gotoNextScreen(DetailsActivity::class.java, item)
        }

        recyclerView.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore(position: Int) {
                presenter?.updatePage()
            }

            override fun onPreLoadMore() {
            }
        })
    }

    override fun updateItemRecycler(posicao: Int) {
        adapter?.notifyItemChanged(posicao)
    }

    override fun updateListRecycler() {
        adapter?.notifyDataSetChanged()
    }

    override fun showErrorSaveOrDeleteTvOnFavorite() {
        DialogBuilder.createDialogYesOrNoDecision(activity,
                getString(R.string.something_went_wrong),
                getString(R.string.on_error_save_or_delete_tv_show_favorite),
                R.string.retry,
                R.string.cancel,
                object : DialogBuilder.ButtonCallback() {
                    override fun onPositive(builder: AlertDialog.Builder?, dialogInterface: DialogInterface?) {
                        presenter?.getShows(null)
                    }
                })
    }

    override fun showDialogOnErrorServer() {
        DialogBuilder.createDialogYesOrNoDecision(activity,
                getString(R.string.something_went_wrong), getString(R.string.server_not_answered),
                R.string.retry,
                R.string.cancel,
                object : DialogBuilder.ButtonCallback() {
                    override fun onPositive(builder: AlertDialog.Builder?, dialogInterface: DialogInterface?) {
                        presenter?.getShows(null)
                    }
                })
    }

    override fun showDialogOnTimeOut() {
        DialogBuilder.createDialogYesOrNoDecision(activity,
                getString(R.string.something_went_wrong), getString(R.string.on_timeout_message),
                R.string.retry,
                R.string.cancel,
                object : DialogBuilder.ButtonCallback() {
                    override fun onPositive(builder: AlertDialog.Builder?, dialogInterface: DialogInterface?) {
                        presenter?.getShows(null)
                    }
                })
    }
}