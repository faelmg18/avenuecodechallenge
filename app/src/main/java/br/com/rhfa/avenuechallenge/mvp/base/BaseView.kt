package br.com.rhfa.avenuechallenge.communication.mvp.base

import br.com.rhfa.avenuechallenge.communication.rest.RHFViewInterface

interface BaseView : RHFViewInterface {

    fun getFragment(): BaseViewFragment?

    fun showProgressBar(visibility: Int)

    fun showToast(message: String)

}