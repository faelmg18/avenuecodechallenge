package br.com.rhfa.avenuechallenge.persistence;

import android.content.Context;

import br.com.rhfa.avenuechallenge.ApplicationContext;

public class RepositoryFactory {

    private static volatile RepositoryFactory instance;
    private static volatile Context context;

    private RepositoryFactory() {
        context = retrieveContext();
    }

    private RepositoryFactory(Context context) {
        this.context = context;
    }

    public static RepositoryFactory getInstance() {

        if (instance == null) {
            instance = new RepositoryFactory();
        }

        return instance;
    }

    public static RepositoryFactory getInstance(Context context) {
        if (instance == null) {
            instance = new RepositoryFactory(context);
        }

        return instance;
    }

    public FavoritesRepository createFavoritesRepository() {
        return FavoritesDao.getInstance(context);
    }


    private Context retrieveContext() {
        if (context == null) {
            return ApplicationContext.Companion.getAppContext();
        } else {
            return context;
        }
    }
}
