package br.com.rhfa.avenuechallenge.persistence;


import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import br.com.rhfa.avenuechallenge.database.BaseDAO;
import br.com.rhfa.avenuechallenge.database.EntitiePersistable;
import br.com.rhfa.avenuechallenge.database.util.DataSerializer;
import br.com.rhfa.avenuechallenge.database.util.DataSerializerImpl;

public abstract class BaseDAOOS<T extends EntitiePersistable> extends BaseDAO<T> {

    protected Context context;

    public BaseDAOOS(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public DataSerializer getDataSerializer() {
        return DataSerializerImpl.getInstance();
    }

    @Override
    public SQLiteOpenHelper getDataBaseHelper() {
        return OpenHelperDataBase.getInstance(getContext());
    }

    public void beginTransaction() {
        dataBase.beginTransaction();
    }

    public void setTransactionSuccessful() {
        dataBase.setTransactionSuccessful();
    }

    public void endTransaction() {
        dataBase.endTransaction();
    }

    public void deleteByIds(long[] ids) {
        String where = generateWhereClause(ids);
        String[] whereArgs = generateWhereArgs(ids);

        delete("id in " + where, whereArgs);
    }

    private String[] generateWhereArgs(long[] ids) {

        String[] whereArgs = new String[ids.length];

        for (int i = 0; i < ids.length; i++) {
            whereArgs[i] = String.valueOf(ids[i]);
        }

        return whereArgs;
    }

    private String generateWhereClause(long[] ids) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("(");

        for (int i = 0; i < ids.length; i++) {

            if (i != 0) {
                stringBuilder.append(",");
            }

            stringBuilder.append("?");
        }

        stringBuilder.append(");");
        String where = stringBuilder.toString();
        return where;
    }
}