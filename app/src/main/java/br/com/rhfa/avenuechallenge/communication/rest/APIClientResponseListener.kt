package br.com.rhfa.avenuechallenge.communication.rest

import retrofit2.Call

interface APIClientResponseListener<T> {
    fun onSuccess(obj: T)
    fun onError(obj: Call<T>, t: ApiClientException)
}
