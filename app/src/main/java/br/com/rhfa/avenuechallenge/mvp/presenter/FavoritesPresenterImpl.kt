package br.com.rhfa.avenuechallenge.mvp.presenter

import android.app.Activity
import android.os.Bundle
import android.view.View
import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseView
import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.mvp.model.FavoritesModel
import br.com.rhfa.avenuechallenge.mvp.model.FavoritesModelImpl
import br.com.rhfa.avenuechallenge.mvp.view.FavoriteView

class FavoritesPresenterImpl : FavoritesPresenter {

    var model: FavoritesModel = FavoritesModelImpl(this)

    var view: FavoriteView? = null
    var items: ArrayList<Show> = ArrayList()

    override fun getFavoritesShows(savedInstanceState: Bundle?) {
        model.getAll()
    }

    override fun updateListRecycler(items: List<Show>) {
        this.items.addAll(items)
        view?.updateListRecycler()
    }

    override fun updateItemRecycler(item: Show) {
        for (i in 0 until this.items.size) {
            if (this.items[i].id === item.id) {
                item.setIsFavorite(item.isFavorite)
                if (model.saveOrRemoveFavorites(item)) {
                    view?.updateItemRecycler(i)
                } else {
                    view?.showErrorSaveOrDeleteTvOnFavorite()
                }

                break
            }
        }
    }

    override fun getFavoritesList(): MutableList<Show> {
        return items
    }

    override fun setView(view: BaseView) {
        this.view = view as FavoriteView
    }

    override fun showToast(message: String) {
        view?.showToast(message)
    }

    override fun showProgressBar(visible: Boolean) {
        val visibility = if (visible) View.VISIBLE else View.GONE
        view!!.showProgressBar(visibility)
    }

    override fun runOnUiThread(action: Runnable) {
        view?.runOnUiThread(action)
    }

    override fun getActivity(): Activity? {
        return view?.getActivity()
    }
}