package br.com.rhfa.avenuechallenge.persistence;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import br.com.rhfa.avenuechallenge.domain.Show;

public class FavoritesDao extends BaseDAOOS<Show> implements FavoritesRepository<Show> {

    private static volatile FavoritesDao instance;

    public FavoritesDao(Context context) {
        super(context);
    }

    public static FavoritesDao getInstance(Context context) {

        if (instance == null) {
            instance = new FavoritesDao(context);
        }

        return instance;
    }

    @Override
    public Class<Show> getEntityClass() {
        return Show.class;
    }

    @Override
    public List<Show> retrieveAllFavorites() {
        return getAllElements();
    }

    @NotNull
    @Override
    public Show retrieveFavoritesById(long id) {
        Show show = getEntitieByID(id);
        return show;
    }
}
