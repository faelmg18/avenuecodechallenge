package br.com.rhfa.avenuechallenge.mvp.model

import br.com.rhfa.avenuechallenge.domain.Show
import br.com.rhfa.avenuechallenge.mvp.presenter.ShowsPresenter
import br.com.rhfa.avenuechallenge.network.ShowServiceImpl
import br.com.rhfa.avenuechallenge.persistence.RepositoryFactory

class ShowsModelImpl(var presenter: ShowsPresenter) : ShowsModel {

    override fun getShows(page: Int) {
        ShowServiceImpl.newInstance(presenter)?.doGetListShow(page)
    }
}