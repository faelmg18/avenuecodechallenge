package br.com.rhfa.avenuechallenge.persistence;

import android.content.Context;

import br.com.rhfa.avenuechallenge.database.DatabaseHelper;
import br.com.rhfa.avenuechallenge.database.ReflectionException;
import br.com.rhfa.avenuechallenge.database.TableUtils;
import br.com.rhfa.avenuechallenge.domain.Show;


public class OpenHelperDataBase extends DatabaseHelper {

    public static final String DATA_BASE_NAME = "aveune_challenge_shows_data_base";
    private static final int DATA_BASE_VERSION = 1;
    private volatile static OpenHelperDataBase instance = null;

    private OpenHelperDataBase(Context context) {
        super(context, DATA_BASE_NAME, DATA_BASE_VERSION);
    }

    public static OpenHelperDataBase getInstance(Context context) {
        if (instance == null) {
            instance = new OpenHelperDataBase(context.getApplicationContext());
        }

        return instance;
    }

    @Override
    public String[] getScriptsCreateDataBase() throws ReflectionException {
        return getScriptCreate();
    }

    @Override
    public String[] getScriptsUpdateDataBase() throws ReflectionException {
        return getScriptUpdate();
    }

    public String[] getScriptCreate() throws ReflectionException {

        String[] script = {
                TableUtils.createTableScript(Show.class)
        };
        return script;
    }

    public String[] getScriptUpdate() throws ReflectionException {
        String[] script = {
                TableUtils.createDropTableScript(Show.class)
        };
        return script;
    }
}
