package br.com.rhfa.avenuechallenge.views.activities

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseView
import br.com.rhfa.avenuechallenge.communication.mvp.base.BaseViewFragment
import br.com.rhfa.avenuechallenge.communication.mvp.basee.BasePresenter


abstract class BaseActivity<out T : BasePresenter> : AppCompatActivity(), BaseView {

    abstract fun myOnCreate()
    abstract fun getLayoutId(): Int
    abstract fun getBaseView(): BaseView

    protected abstract fun newPresenter(): T
    private var presenter: T? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getLayoutId())

        if (presenter == null) {
            presenter = newPresenter()
        }

        presenter!!.setView(getBaseView())

        myOnCreate()
    }

    fun addBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun getPresenter(): T {
        return this.presenter!!
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun getActivity(): Activity? {
        return this
    }

    override fun getFragment(): BaseViewFragment? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showProgressBar(visibility: Int) {

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}