package br.com.rhfa.avenuechallenge.views.components

interface OnLoadMoreListener {
    fun onLoadMore(position: Int)

    fun onPreLoadMore()
}
