package br.com.rhfa.avenuechallenge.domain;

import br.com.rhfa.avenuechallenge.database.EntitiePersistable;

public class Channel implements EntitiePersistable {

    private long id;
    private String name;
    private Country country;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
