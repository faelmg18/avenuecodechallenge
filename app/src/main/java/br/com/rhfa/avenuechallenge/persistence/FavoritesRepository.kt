package br.com.rhfa.avenuechallenge.persistence

import br.com.rhfa.avenuechallenge.domain.Show

interface FavoritesRepository<T> : BaseRepository<T> {

    fun delete(item: T): Long
    fun save(item: T): Long
    fun retrieveAllFavorites(): List<Show>
    fun retrieveFavoritesById(id: Long): Show
}
