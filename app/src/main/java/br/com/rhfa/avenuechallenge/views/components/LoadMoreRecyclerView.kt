package br.com.rhfa.avenuechallenge.views.components

import android.content.Context
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.util.Log

class LoadMoreRecyclerView : RecyclerView {

    private var loading = true
    internal var pastVisiblesItems: Int = 0
    internal var visibleItemCount: Int = 0
    internal var totalItemCount: Int = 0
    //WrapperLinearLayout is for handling crash in RecyclerView
    private var mLayoutManager: WrapperLinearLayout? = null
    private var mContext: Context? = null
    private var onLoadMoreListener: OnLoadMoreListener? = null

    constructor(context: Context) : super(context) {
        mContext = context
        init()
    }

    override fun setLayoutManager(layout: RecyclerView.LayoutManager?) {
        super.setLayoutManager(layout)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        mContext = context
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        mContext = context
        init()
    }

    private fun init() {
        mLayoutManager = WrapperLinearLayout(mContext!!, LinearLayoutManager.VERTICAL, false)
        this.layoutManager = mLayoutManager
        this.itemAnimator = DefaultItemAnimator()
        this.setHasFixedSize(true)
    }

    override fun onScrolled(dx: Int, dy: Int) {
        super.onScrolled(dx, dy)

        if (dy > 0) {
            visibleItemCount = mLayoutManager!!.childCount
            totalItemCount = mLayoutManager!!.itemCount
            pastVisiblesItems = mLayoutManager!!.findFirstVisibleItemPosition()

            if (loading) {
                if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                    loading = false
                    Log.v("...", "Call Load More !")
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener!!.onPreLoadMore()
                        onLoadMoreListener!!.onLoadMore(mLayoutManager!!.findLastVisibleItemPosition())
                    }
                }
            }
        }
    }

    override fun onScrollStateChanged(state: Int) {
        super.onScrollStateChanged(state)
    }

    fun enableLoadingMore(moreLoading: Boolean) {
        loading = moreLoading
    }

    fun setOnLoadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

    fun setScrolling(enable: Boolean) {
        mLayoutManager!!.setScrollEnabled(enable)
    }
}
